# README #

It's simple rest service that support

* CRUD operations for account
* transfer operation between accounts

Version 1.0

### Requires ###

* Java 1.8

### How to run ###

* Download [simplerestserver-1.0-SNAPSHOT.jar](https://bitbucket.org/timofeidyn/simplerest/downloads/simplerestserver-1.0-SNAPSHOT.jar)
* Execute `java -jar simplerestserver-1.0-SNAPSHOT.jar`

### REST API ###

* GET: `/user/{userId}/accounts` - get all users accounts
+ POST: `/user/{userId}/accounts` - create new user account
  > Content: `{"name": {account_name}, "balance": {initial balance}}`
* GET: `/user/{userId}/accounts/{accountId}` - get user account
+ PUT: `/user/{userId}/accounts/{accountId}` - update account
  > Content: `{"name": {account_name}, "balance": {new balance}}`
* DELETE: `/user/{userId}/accounts/{accountId}` - delete user account
+ POST: `/user/{userId}/accounts/{accountId}/transfer` - make transfer from user account
  > Content: `{"destinationAccountId": "{id}", "amount": {amount}}`

### How to build ###

* `mvn compile` - compile
* `mvn test` - build and test
* `mvn package` - build and pack
* `mvn exec:java` - build and run