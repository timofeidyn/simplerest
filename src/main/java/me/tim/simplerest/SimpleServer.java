package me.tim.simplerest;

import io.undertow.Undertow;
import io.undertow.server.RoutingHandler;

import java.io.IOException;

public class SimpleServer {
    public static final String HOST = "localhost";
    public static final int PORT = 8080;

    public static Undertow configureServer(String host, int port, RoutingHandler routs) {
        return Undertow.builder()
                .addHttpListener(port, host)
                .setHandler(routs).build();
    }

    public static void main(final String[] args) throws IOException {
        Undertow server = configureServer(HOST, PORT, AccountsResources.ROUTS);
        server.start();
        System.out.println("Server started. http://" + HOST + ":" + PORT + "/\n" +
                "Press enter for stop...");
        System.in.read();
        server.stop();
    }
}
