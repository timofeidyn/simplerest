package me.tim.simplerest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account implements Serializable {
    private UUID id;
    private UUID ownerId;
    private String name;
    private BigDecimal balance;
    private long version = 0;

    public Account clone() {
        return new Account(id, ownerId, name, balance, version);
    }
}

