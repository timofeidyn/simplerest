package me.tim.simplerest;


import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

public class AccountsDAO {
    private final ConcurrentHashMap<UUID, Account> accountsMap = new ConcurrentHashMap<>();
    private final ConcurrentSkipListSet<UUID> writeLocks = new ConcurrentSkipListSet<>();

    private static class SingletonHolder {
        public static final AccountsDAO INSTANCE = new AccountsDAO();
    }

    public static AccountsDAO getInstance() {
        return SingletonHolder.INSTANCE;
    }


    private AccountsDAO() {
    }

    public Account create(UUID ownerId, String name, BigDecimal balance) {
        Account account = new Account(UUID.randomUUID(), ownerId, name, balance, 0);
        accountsMap.put(account.getId(), account);
        return account.clone();
    }

    private Account internalUpdate(Account account) {
        Account oldAccount = accountsMap.get(account.getId());
        if (oldAccount == null)
            throw new IllegalArgumentException("Account not found: " + account);
        if (account.getVersion() == oldAccount.getVersion()) {
            account = account.clone();
            account.setVersion(account.getVersion() + 1);
            accountsMap.replace(account.getId(), account);
        } else {
            throw new ConcurrentModificationException();
        }
        return account;
    }

    public Account update(Account account) {
        if (writeLocks.add(account.getId())) {
            try {
                account = internalUpdate(account);
            } finally {
                writeLocks.remove(account.getId());
            }
        } else {
            throw new ConcurrentModificationException();
        }
        return account;
    }

    public void updateAll(List<Account> accounts) {
        List<UUID> lockedIds = accounts.stream().map(Account::getId)
                .filter(writeLocks::add).collect(Collectors.toList());
        try {
            if (lockedIds.size() == accounts.size()
                    && accounts.stream().allMatch(acc -> acc.getVersion() == accountsMap.get(acc.getId()).getVersion())) {
                accounts.forEach(this::internalUpdate);
            } else {
                throw new ConcurrentModificationException();
            }
        } finally {
            writeLocks.removeAll(lockedIds);
        }
    }

    public Optional<Account> get(UUID id) {
        return Optional.ofNullable(accountsMap.get(id)).map(Account::clone);
    }

    public Optional<Account> delete(UUID id) {
        Optional<Account> result;
        if (writeLocks.add(id)) {
            result = Optional.ofNullable(accountsMap.remove(id));
            writeLocks.remove(id);
        } else {
            throw new ConcurrentModificationException();
        }
        return result;
    }

    public List<Account> getUserAccounts(UUID userId) {
        return accountsMap.values().stream()
                .filter(account -> userId.equals(account.getOwnerId()))
                .map(Account::clone)
                .collect(Collectors.toList());
    }

    public Optional<Account> getUserAccount(UUID ownerId, UUID accountId) {
        return get(accountId).filter(account -> ownerId.equals(account.getOwnerId()));
    }
}

