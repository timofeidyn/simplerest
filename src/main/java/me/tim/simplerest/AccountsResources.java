package me.tim.simplerest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;
import io.undertow.Handlers;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.RoutingHandler;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

public class AccountsResources {
    public static final String ROOT_URL = "/user/{userId}/accounts";
    public static final RoutingHandler ROUTS = Handlers.routing()
            .get(ROOT_URL, AccountsResources::getAccounts)
            .post(ROOT_URL, AccountsResources::createAccount)
            .get(ROOT_URL + "/{accountId}", AccountsResources::getAccount)
            .put(ROOT_URL + "/{accountId}", AccountsResources::updateAccount)
            .delete(ROOT_URL + "/{accountId}", AccountsResources::deleteAccount)
            .post(ROOT_URL + "/{accountId}/transfer", AccountsResources::makeTransfer);

    private static final AccountsDAO accountsStorage = AccountsDAO.getInstance();
    private static final ObjectMapper jsonMapper = new ObjectMapper();

    static {
        jsonMapper.registerModule(new AfterburnerModule());
    }

    public static void getAccounts(HttpServerExchange exchange) {
        errorHandler(exchange, () -> {
            UUID ownerId = getRequiredQueryParameterAsUUID(exchange, "userId");
            List<Account> accounts = accountsStorage.getUserAccounts(ownerId);
            String jsonString = jsonMapper.writeValueAsString(accounts);
            exchange.getResponseSender().send(jsonString);
        });
    }

    public static void getAccount(HttpServerExchange exchange) {
        errorHandler(exchange, () -> {
            UUID accountId = getRequiredQueryParameterAsUUID(exchange, "accountId");
            UUID ownerId = getRequiredQueryParameterAsUUID(exchange, "userId");

            Optional<Account> accountOptional = accountsStorage.getUserAccount(ownerId, accountId);
            if (accountOptional.isPresent()) {
                String jsonString = jsonMapper.writeValueAsString(accountOptional.get());
                exchange.getResponseSender().send(jsonString);
            } else {
                exchange.setStatusCode(StatusCodes.NOT_FOUND);
            }
        });
    }

    public static void createAccount(HttpServerExchange exchange) throws IOException {
        if (checkNonBlockingThread(exchange, AccountsResources::createAccount)) return;
        errorHandler(exchange, () -> {
            UUID ownerId = getRequiredQueryParameterAsUUID(exchange, "userId");
            exchange.startBlocking();
            Account parsedAccount = jsonMapper.readValue(exchange.getInputStream(), Account.class);
            String name = Optional.ofNullable(parsedAccount.getName()).orElseThrow(IllegalArgumentException::new);
            BigDecimal balance = Optional.ofNullable(parsedAccount.getBalance()).orElseThrow(IllegalArgumentException::new);

            Account newAccount = accountsStorage.create(ownerId, parsedAccount.getName(), parsedAccount.getBalance());

            String jsonString = jsonMapper.writeValueAsString(newAccount);
            exchange.getResponseSender().send(jsonString);
        });
    }

    public static void updateAccount(HttpServerExchange exchange) {
        if (checkNonBlockingThread(exchange, AccountsResources::updateAccount)) return;
        errorHandler(exchange, () -> {
            UUID ownerId = getRequiredQueryParameterAsUUID(exchange, "userId");
            UUID accountId = getRequiredQueryParameterAsUUID(exchange, "accountId");
            exchange.startBlocking();
            Account parsedAccount = jsonMapper.readValue(exchange.getInputStream(), Account.class);

            Account account = accountsStorage.getUserAccount(ownerId, accountId)
                    .orElseThrow(IllegalArgumentException::new);
            account.setName(parsedAccount.getName());
            account.setBalance(parsedAccount.getBalance());
            account = accountsStorage.update(account);
            String jsonString = jsonMapper.writeValueAsString(account);
            exchange.getResponseSender().send(jsonString);
        });
    }

    public static void deleteAccount(HttpServerExchange exchange) {
        errorHandler(exchange, () -> {
            UUID accountId = getRequiredQueryParameterAsUUID(exchange, "accountId");
            UUID ownerId = getRequiredQueryParameterAsUUID(exchange, "userId");

            Account account = accountsStorage.getUserAccount(ownerId, accountId)
                    .orElseThrow(IllegalArgumentException::new);

            accountsStorage.delete(accountId);
            String jsonString = jsonMapper.writeValueAsString(account);
            exchange.getResponseSender().send(jsonString);
        });
    }

    public static void makeTransfer(HttpServerExchange exchange) {
        if (checkNonBlockingThread(exchange, AccountsResources::makeTransfer)) return;
        errorHandler(exchange, () -> {
            exchange.startBlocking();
            JsonNode bodyParams = jsonMapper.readTree(exchange.getInputStream());
            String destinationAccountIdParameter = getRequiredBodyParameter(bodyParams, "destinationAccountId").asText();
            UUID destinationAccountId = UUID.fromString(destinationAccountIdParameter);
            BigDecimal amount = getRequiredBodyParameter(bodyParams, "amount").decimalValue();
            UUID ownerId = getRequiredQueryParameterAsUUID(exchange, "userId");
            UUID accountId = getRequiredQueryParameterAsUUID(exchange, "accountId");

            Account account = accountsStorage.getUserAccount(ownerId, accountId).orElseThrow(IllegalArgumentException::new);
            Account destinationAccount = accountsStorage.get(destinationAccountId).orElseThrow(IllegalArgumentException::new);

            BigDecimal accountBalance = account.getBalance();
            if (BigDecimal.ZERO.compareTo(amount) < 0 && accountBalance.compareTo(amount) > 0) {
                BigDecimal destinationAccountBalance = destinationAccount.getBalance();
                account.setBalance(accountBalance.subtract(amount));
                destinationAccount.setBalance(destinationAccountBalance.add(amount));

                accountsStorage.updateAll(Arrays.asList(account, destinationAccount));
                String jsonString = jsonMapper.writeValueAsString(account);
                exchange.getResponseSender().send(jsonString);
            } else {
                exchange.setStatusCode(StatusCodes.FORBIDDEN);
            }
        });
    }

    // helpers
    @FunctionalInterface
    private interface functionalWithException {
        void execute() throws Exception;
    }

    private static void errorHandler(HttpServerExchange exchange, functionalWithException func) {
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
        try {
            func.execute();
        } catch (JsonProcessingException | IllegalArgumentException e) {
            if (exchange.isResponseChannelAvailable()) {
                exchange.setStatusCode(StatusCodes.BAD_REQUEST).endExchange();
            }
        } catch (ConcurrentModificationException e) {
            if (exchange.isResponseChannelAvailable()) {
                exchange.setStatusCode(StatusCodes.CONFLICT).endExchange();
            }
        } catch (Exception e) {
            if (exchange.isResponseChannelAvailable()) {
                exchange.setStatusCode(StatusCodes.INTERNAL_SERVER_ERROR).endExchange();
            }
        }
    }

    private static boolean checkNonBlockingThread(HttpServerExchange exchange, HttpHandler handler) {
        if (exchange.isInIoThread()) {
            exchange.dispatch(handler);
            return true;
        }
        return false;
    }

    public static UUID getRequiredQueryParameterAsUUID(HttpServerExchange exchange, String key) {
        String parameter = Optional.ofNullable(exchange.getQueryParameters().get(key))
                .map(Deque::getFirst)
                .orElseThrow(() -> new IllegalArgumentException(key + " parameter is absent"));
        return UUID.fromString(parameter);
    }

    public static JsonNode getRequiredBodyParameter(JsonNode bodyParams, String key) {
        Optional<JsonNode> parameter = Optional.ofNullable(bodyParams.get(key));
        return parameter.orElseThrow(() -> new IllegalArgumentException(key + " parameter is absent"));
    }
}
