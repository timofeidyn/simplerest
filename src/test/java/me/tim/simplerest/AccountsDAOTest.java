package me.tim.simplerest;

import org.junit.Test;
import java.math.BigDecimal;
import java.util.*;
import static org.junit.Assert.*;

public class AccountsDAOTest {
    public static final AccountsDAO dao = AccountsDAO.getInstance();

    @Test
    public void createAndGetAndDelete() throws Exception {
        Account account1 = dao.create(UUID.randomUUID(), "account1", BigDecimal.valueOf(100));

        Optional<Account> accountFromDAO = dao.get(account1.getId());
        assertTrue(accountFromDAO.isPresent());
        assertEquals(account1, accountFromDAO.get());

        Account deletedAccount = dao.delete(account1.getId()).get();
        assertEquals(account1, deletedAccount);
    }

    @Test
    public void getNullable() throws Exception {
        Optional<Account> accountFromDAO = dao.get(UUID.randomUUID());
        assertFalse(accountFromDAO.isPresent());
    }

    @Test
    public void deleteNullable() throws Exception {
        Optional<Account> account = dao.delete(UUID.randomUUID());
        assertFalse(account.isPresent());
    }

    @Test
    public void update() throws Exception {
        Account account1 = dao.create(UUID.randomUUID(), "account1", BigDecimal.valueOf(100));
        account1.setName("account2");
        account1.setBalance(BigDecimal.TEN);

        Account updatedAccount = dao.update(account1);

        assertEquals(1, updatedAccount.getVersion());
        assertEquals("account2", updatedAccount.getName());
        assertEquals(BigDecimal.TEN, updatedAccount.getBalance());

    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNonExistedAccount() throws Exception {
        dao.update(new Account(UUID.randomUUID(), UUID.randomUUID(), "acc", BigDecimal.ZERO, 0));
    }

    @Test(expected = ConcurrentModificationException.class)
    public void updateWithConflict() throws Exception {
        Account account1 = dao.create(UUID.randomUUID(), "account1", BigDecimal.valueOf(100));
        Account account2 = dao.get(account1.getId()).get();
        dao.update(account1);
        dao.update(account2);
    }

    @Test
    public void updateAll() throws Exception {
        Account account1 = dao.create(UUID.randomUUID(), "account1", BigDecimal.valueOf(100));
        Account account2 = dao.create(UUID.randomUUID(), "account2", BigDecimal.valueOf(100));
        dao.updateAll(Arrays.asList(account1, account2));

        Account updatedAcc1 = dao.get(account1.getId()).get();
        Account updatedAcc2 = dao.get(account2.getId()).get();
        account1.setVersion(account1.getVersion()+1);
        account2.setVersion(account2.getVersion()+1);
        assertEquals(account1, updatedAcc1);
        assertEquals(account2, updatedAcc2);
    }

    @Test
    public void updateAllWithConflict() throws Exception {
        Account account1 = dao.create(UUID.randomUUID(), "account1", BigDecimal.valueOf(100));
        Account account2 = dao.create(UUID.randomUUID(), "account2", BigDecimal.valueOf(100));
        dao.update(account2.clone()); // make conflict update

        try {
            dao.updateAll(Arrays.asList(account1, account2));
            fail("ConcurrentModificationException should be thrown");
        } catch (ConcurrentModificationException e) {
            Account notUpdatedAcc1 = dao.get(account1.getId()).get();
            Account updatedAcc2 = dao.get(account2.getId()).get();
            assertEquals(account1, notUpdatedAcc1);
            assertNotEquals(account2, updatedAcc2);
        }
    }

    @Test
    public void getUserAccounts() throws Exception {
        UUID userId = UUID.randomUUID();
        Account account1 = dao.create(userId, "account1", BigDecimal.ZERO);
        Account account2 = dao.create(userId, "account2", BigDecimal.ONE);
        Account nonUserAccount = dao.create(UUID.randomUUID(), "nonUserAccount", BigDecimal.ONE);

        List<Account> accounts = dao.getUserAccounts(userId);
        assertEquals(2, accounts.size());
    }

}