package me.tim.simplerest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.undertow.Undertow;
import io.undertow.util.StatusCodes;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class AccountsResourcesTest {
    private static Undertow server;
    private static CloseableHttpClient httpclient;
    private static final ObjectMapper jsonMapper = new ObjectMapper();

    private static final String ENTRY_POINT_TEMPLATE = "http://localhost:8081/user/%s/accounts";

    @BeforeClass
    public static void setUp() throws Exception {
        server = SimpleServer.configureServer("localhost", 8081, AccountsResources.ROUTS);
        server.start();
        httpclient = HttpClients.createDefault();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void createAccout() throws Exception {
        UUID ownerId = UUID.randomUUID();
        String url = String.format(ENTRY_POINT_TEMPLATE, ownerId);
        HttpPost request = new HttpPost(url);
        String body = "{\"name\": \"account1\", \"balance\":1}";
        request.setEntity(new StringEntity(body));
        HttpResponse response = httpclient.execute(request);

        assertEquals(StatusCodes.OK, response.getStatusLine().getStatusCode());
        JsonNode jsonContent = jsonMapper.readTree(response.getEntity().getContent());
        assertEquals(jsonContent.get("name").asText(), "account1");
        assertEquals(jsonContent.get("balance").decimalValue(), BigDecimal.ONE);
        assertEquals(jsonContent.get("ownerId").asText(), ownerId.toString());
    }

    @Test
    public void createAccoutWithIncorrectJsonBody() throws Exception {
        String url = String.format(ENTRY_POINT_TEMPLATE, UUID.randomUUID());
        HttpPost request = new HttpPost(url);

        request.setEntity(new StringEntity("{\"name\": \"acco"));
        HttpResponse response = httpclient.execute(request);
        assertEquals(StatusCodes.BAD_REQUEST, response.getStatusLine().getStatusCode());
    }

    @Test
    public void createAccoutWithoutRequiredParams() throws Exception {
        String url = String.format(ENTRY_POINT_TEMPLATE, UUID.randomUUID());
        HttpPost request = new HttpPost(url);

        request.setEntity(new StringEntity("{\"name\": \"acc1\"}"));
        HttpResponse response = httpclient.execute(request);
        assertEquals(StatusCodes.BAD_REQUEST, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getAccount() throws Exception {
        Account testAccount = AccountsDAO.getInstance().create(UUID.randomUUID(), "testAcc", BigDecimal.TEN);

        String url = String.format("http://localhost:8081/user/%s/accounts/%s", testAccount.getOwnerId(), testAccount.getId());
        HttpUriRequest request = new HttpGet(url);
        HttpResponse response = httpclient.execute(request);
        String content = EntityUtils.toString(response.getEntity());
        Account actualAccount = jsonMapper.readValue(content, Account.class);

        assertEquals(testAccount, actualAccount);
    }

    @Test
    public void getNotOwnAccount() throws Exception {
        Account testAccount = AccountsDAO.getInstance().create(UUID.randomUUID(), "testAcc", BigDecimal.TEN);

        String url = String.format("http://localhost:8081/user/%s/accounts/%s", UUID.randomUUID(), testAccount.getId());
        HttpUriRequest request = new HttpGet(url);
        HttpResponse response = httpclient.execute(request);

        assertEquals(StatusCodes.NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getAccounts() throws Exception {
        UUID userId = UUID.randomUUID();
        Account testAccount1 = AccountsDAO.getInstance().create(userId, "testAcc1", BigDecimal.TEN);
        Account testAccount2 = AccountsDAO.getInstance().create(userId, "testAcc2", BigDecimal.ONE);

        String url = String.format(ENTRY_POINT_TEMPLATE, userId);
        HttpUriRequest request = new HttpGet(url);
        HttpResponse response = httpclient.execute(request);
        String content = EntityUtils.toString(response.getEntity());
        Account[] accounts = jsonMapper.readValue(content, Account[].class);

        assertEquals(2, accounts.length);
        for(Account account: accounts) {
            if (testAccount1.getId().equals(account.getId())) {
                assertEquals(testAccount1, account);
            } else if (testAccount2.getId().equals(account.getId())) {
                assertEquals(testAccount2, account);
            } else {
                fail("Account {"+account+"} not matched with test data");
            }
        }
    }

    @Test
    public void getAccountsEmptyList() throws Exception {
        UUID userId = UUID.randomUUID();
        String url = String.format(ENTRY_POINT_TEMPLATE, userId);
        HttpUriRequest request = new HttpGet(url);
        HttpResponse response = httpclient.execute(request);
        String content = EntityUtils.toString(response.getEntity());
        Account[] accounts = jsonMapper.readValue(content, Account[].class);

        assertEquals(0, accounts.length);
    }

    @Test
    public void deleteAccount() throws IOException {
        Account testAccount = AccountsDAO.getInstance().create(UUID.randomUUID(), "testAcc", BigDecimal.TEN);

        String url = String.format("http://localhost:8081/user/%s/accounts/%s", testAccount.getOwnerId(), testAccount.getId());
        HttpUriRequest request = new HttpDelete(url);
        HttpResponse response = httpclient.execute(request);
        String content = EntityUtils.toString(response.getEntity());
        Account actualAccount = jsonMapper.readValue(content, Account.class);

        assertEquals(testAccount, actualAccount);
    }

    @Test
    public void deleteNonexistentAccount() throws Exception {
        String url = String.format("http://localhost:8081/user/%s/accounts/%s", UUID.randomUUID(), UUID.randomUUID());
        HttpUriRequest request = new HttpDelete(url);
        HttpResponse response = httpclient.execute(request);

        assertEquals(StatusCodes.BAD_REQUEST, response.getStatusLine().getStatusCode());
    }

    @Test
    public void updateAccount() throws Exception {
        Account testAccount = AccountsDAO.getInstance().create(UUID.randomUUID(), "testAcc", BigDecimal.TEN);
        testAccount.setName("newName");
        testAccount.setBalance(BigDecimal.ONE);

        String url = String.format("http://localhost:8081/user/%s/accounts/%s", testAccount.getOwnerId(), testAccount.getId());
        HttpPut request = new HttpPut(url);
        String body = jsonMapper.writeValueAsString(testAccount);
        request.setEntity(new StringEntity(body));
        HttpResponse response = httpclient.execute(request);
        String content = EntityUtils.toString(response.getEntity());
        Account actualAccount = jsonMapper.readValue(content, Account.class);

        testAccount.setVersion(testAccount.getVersion()+1);
        assertEquals(testAccount, actualAccount);
    }

    @Test
    public void makeTransfer() throws Exception {
        AccountsDAO dao = AccountsDAO.getInstance();
        Account from = dao.create(UUID.randomUUID(), "accFrom", BigDecimal.TEN);
        Account to = dao.create(UUID.randomUUID(), "accTo", BigDecimal.TEN);

        String url = String.format("http://localhost:8081/user/%s/accounts/%s/transfer", from.getOwnerId(), from.getId());
        HttpPost request = new HttpPost(url);
        String body = String.format("{\"destinationAccountId\": \"%s\", \"amount\": 1}", to.getId());
        request.setEntity(new StringEntity(body));
        HttpResponse response = httpclient.execute(request);
        String content = EntityUtils.toString(response.getEntity());
        Account actualAccount = jsonMapper.readValue(content, Account.class);

        assertEquals(BigDecimal.valueOf(9), actualAccount.getBalance());
        Account newFrom = dao.get(from.getId()).get();
        assertEquals(BigDecimal.valueOf(9), newFrom.getBalance());
        Account newTo = dao.get(to.getId()).get();
        assertEquals(BigDecimal.valueOf(11), newTo.getBalance());
    }

    @Test
    public void makeToAllowedTransfer() throws Exception {
        AccountsDAO dao = AccountsDAO.getInstance();
        Account from = dao.create(UUID.randomUUID(), "accFrom", BigDecimal.TEN);
        Account to = dao.create(UUID.randomUUID(), "accTo", BigDecimal.TEN);

        String url = String.format("http://localhost:8081/user/%s/accounts/%s/transfer", from.getOwnerId(), from.getId());
        HttpPost request = new HttpPost(url);
        String body = String.format("{\"destinationAccountId\": \"%s\", \"amount\": 11}", to.getId());
        request.setEntity(new StringEntity(body));
        HttpResponse response = httpclient.execute(request);

        assertEquals(StatusCodes.FORBIDDEN, response.getStatusLine().getStatusCode());
        Account newFrom = dao.get(from.getId()).get();
        assertEquals(from.getBalance(), newFrom.getBalance());
        Account newTo = dao.get(to.getId()).get();
        assertEquals(to.getBalance(), newTo.getBalance());
    }
}